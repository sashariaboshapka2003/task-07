package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    private Connection connect() {
        try {
            return DriverManager.getConnection(loadProperties().getProperty("connection.url"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Properties loadProperties() {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream("app.properties")) {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public List<User> findAllUsers() throws DBException {
        try (Connection con = connect();
             PreparedStatement pr = Objects.requireNonNull(con).prepareStatement("SELECT * FROM users")) {
            ResultSet rs = pr.executeQuery();
            List<User> users = new ArrayList<>();

            while (rs.next()) {
                users.add(new User(rs.getInt("id"), rs.getString("login")));
            }

            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        if (user == null) {
            return false;
        }
        try (Connection con = connect();
             Statement st = Objects.requireNonNull(con).createStatement()) {
            if (1 == st.executeUpdate("INSERT INTO users (login) VALUES ('" + user.getLogin() + "')", Statement.RETURN_GENERATED_KEYS)) {
                try (ResultSet rs = st.getGeneratedKeys()) {
                    rs.next();
                    user.setId(rs.getInt(1));
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        if (users.length == 0) {
            return false;
        }
        StringJoiner sj = new StringJoiner(",", "DELETE FROM users WHERE id IN (", ")");
        for (User u : users) {
            if (u != null) {
                sj.add(String.valueOf(u.getId()));
            }
        }
        try (Connection con = connect();
             Statement st = Objects.requireNonNull(con).createStatement()) {
            return st.executeUpdate(sj.toString()) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public User getUser(String login) throws DBException {
        if (login == null) {
            return null;
        }
        try (Connection con = connect();
             ResultSet rs = Objects.requireNonNull(con).createStatement().executeQuery("SELECT id, login FROM users WHERE login = '" + login + "'")) {
            if (rs.next()) {
                return new User(rs.getInt("id"), rs.getString("login"));
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public Team getTeam(String name) throws DBException {
        if (name == null) {
            return null;
        }
        try (Connection con = connect();
             ResultSet rs = Objects.requireNonNull(con).createStatement().executeQuery("SELECT id, name FROM teams WHERE name = '" + name + "'")) {
            if (rs.next()) {
                return new Team(rs.getInt("id"), rs.getString("name"));
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = connect();
             ResultSet rs = Objects.requireNonNull(con).createStatement().executeQuery("SELECT id, name FROM teams")) {
            while (rs.next()) {
                teams.add(new Team(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }
        try (Connection con = connect();
             Statement st = Objects.requireNonNull(con).createStatement()) {
            if (1 == st.executeUpdate("INSERT INTO teams (name) VALUES ('" + team.getName() + "')", Statement.RETURN_GENERATED_KEYS)) {
                try (ResultSet rs = st.getGeneratedKeys()) {
                    rs.next();
                    team.setId(rs.getInt(1));
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null || teams.length == 0) {
            return false;
        }
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = connect();
            st = Objects.requireNonNull(con).prepareStatement("INSERT INTO users_teams (user_id,team_id) VALUES(" + user.getId() + ",?)");
            con.setAutoCommit(false);
            boolean isInserted = false;
            for (Team t : teams) {
                if (t != null) {
                    st.setInt(1, t.getId());
                    isInserted = (st.executeUpdate() > 0) || isInserted;
                }
            }
            con.commit();
            return isInserted;
        } catch (SQLException e) {
            e.printStackTrace();
            tryRollback(con);
            throw new DBException("", e);
        } finally {
            tryClose(st);
            tryClose(con);
        }
    }

    private void tryRollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void tryClose(AutoCloseable con) {
        if (con != null) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = connect();
             ResultSet rs = Objects.requireNonNull(con).createStatement().executeQuery("SELECT team_id, name FROM users_teams JOIN teams ON team_id = id WHERE user_id = " + user.getId())) {
            while (rs.next()) {
                teams.add(new Team(rs.getInt("team_id"), rs.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }
        try (Connection con = connect();
             Statement st = Objects.requireNonNull(con).createStatement()) {
            return st.executeUpdate("DELETE FROM teams WHERE name = '" + team.getName() + "'") > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        if (team == null) {
            return false;
        }
        try (Connection con = connect();
             Statement st = Objects.requireNonNull(con).createStatement()) {
            return st.executeUpdate("UPDATE teams SET name = '" + team.getName() + "' WHERE id = " + team.getId()) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e);
        }
    }
}
